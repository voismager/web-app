export default {
    s3: {
        REGION: "us-east-1",
        BUCKET: "back-to-you-files"
    },

    apiGateway: {
        REGION: "us-east-1",
        URL: "https://jvp9pggwyc.execute-api.us-east-1.amazonaws.com/prod"
    },

    cognito: {
        REGION: "us-east-1",
        USER_POOL_ID: "us-east-1_xDUnQ7omQ",
        APP_CLIENT_ID: "4dcf957ua3ttjq1e49uls3jevk",
        IDENTITY_POOL_ID: "us-east-1:666c33ea-f0ca-4fbb-8149-6eb1fd875c3b"
    },

    app: {
        MIN_USERNAME_L: 6,
        MAX_USERNAME_L: 127,
        MAX_EMAIL_L: 254,
        MIN_PASSWORD_L: 8,
        MAX_PASSWORD_L: 255,
        MAX_COMMENT_L: 240
    }
};