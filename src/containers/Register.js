import React, { Component } from 'react';
import { Form, FormGroup, Input, Button, Label, FormText } from 'reactstrap';
import { Auth } from "aws-amplify";
import config from '../config';

export default class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            username: "",
            password: "",
            confirmPassword: "",
            confirmationCode: "",
            newUser: null,

            validate: {
                email: '',
                username: '',
                password: '',
                confirmPassword: ''
            },

            serverError: null
        };
    }

    validateUsername(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length >= config.app.MIN_USERNAME_L &&
            value.length <= config.app.MAX_USERNAME_L &&
            /^[A-Za-z0-9_]+$/.test(value)) {

            validate.username = 'has-success';
        }

        else validate.username = 'has-danger';

        this.setState({ validate });
    }

    validateEmail(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length <= config.app.MAX_EMAIL_L &&
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) {

            validate.email = 'has-success';
        }

        else validate.email = 'has-danger';

        this.setState({ validate });
    }

    validatePassword(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length >= config.app.MIN_PASSWORD_L &&
            value.length <= config.app.MAX_PASSWORD_L &&
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]*$/.test(value)) {

            validate.password = 'has-success';
        }

        else validate.password = 'has-danger';

        this.setState({ validate });
    }

    validateConfirmPassword(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value === this.state.password) {
            validate.confirmPassword = 'has-success';
        }

        else validate.confirmPassword = 'has-danger';

        this.setState({ validate });
    }

    validateRegistrationForm = () => {
        const { validate } = this.state;

        return validate.username === 'has-success'
            && validate.email === 'has-success'
            && validate.password === 'has-success'
            && validate.confirmPassword === 'has-success';
    };

    validateConfirmationForm = () => {
        return this.state.confirmationCode.length > 0;
    };

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value,
            serverError: null
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        try {
            const newUser = await Auth.signUp({
                username: this.state.username,
                password: this.state.password,

                attributes: {
                    email: this.state.email
                }
            });

            this.setState({
                newUser
            });
        }

        catch (e) {
            switch (e.name) {
                case 'UsernameExistsException':
                    //Auth.resendSignUp(this.state.username);

                    this.setState({
                        serverError: "Username is already taken!"
                    });

                    break;

                case 'UserLambdaValidationException':
                    this.setState({
                        serverError: "Email already exists."
                    });

                    break;

                default:
                    this.setState({
                        serverError: e.message
                    });

                    break;
            }
        }
    };

    handleConfirmationSubmit = async event => {
        event.preventDefault();

        try {
            await Auth.confirmSignUp(this.state.username, this.state.confirmationCode, { forceAliasCreation: false });
            const result = await Auth.signIn(this.state.username, this.state.password);
            this.props.userHasAuthenticated(result.username);
            this.props.history.push("/");
        }

        catch (e) {
            this.setState({
                serverError: e.message
            });
        }
    };

    renderConfirmationForm() {
        return (
            <Form onSubmit={this.handleConfirmationSubmit}>
                <FormGroup>
                    <Label>Confirmation Code</Label>
                    <Input
                        type="tel"
                        name="confirmationCode"
                        value={this.state.confirmationCode}
                        onChange={this.handleChange}
                    />
                    <FormText>Please check your email for the code.</FormText>
                </FormGroup>

                <Button disabled={!this.validateConfirmationForm()}>Verify</Button>
            </Form>
        );
    }

    renderForm() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormGroup>
                    <Label>Email</Label>
                    <Input
                        type="email"
                        name="email"
                        value={this.state.email}
                        onChange={(e) => {
                            this.validateEmail(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.email === 'has-success'}
                        invalid={this.state.validate.email === 'has-danger'}
                    />
                </FormGroup>

                <FormGroup>
                    <Label>Username</Label>
                    <Input
                        type="text"
                        name="username"
                        value={this.state.username}
                        onChange={(e) => {
                            this.validateUsername(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.username === 'has-success'}
                        invalid={this.state.validate.username === 'has-danger'}
                    />

                    <FormText>Should be at least {config.app.MIN_USERNAME_L} characters.</FormText>
                </FormGroup>

                <FormGroup>
                    <Label>Password</Label>
                    <Input
                        type="password"
                        name="password"
                        value={this.state.password}
                        onChange={(e) => {
                            this.validatePassword(e);
                            this.validateConfirmPassword(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.password === 'has-success'}
                        invalid={this.state.validate.password === 'has-danger'}
                    />
                </FormGroup>

                <FormGroup>
                    <Label>Confirm Password</Label>
                    <Input
                        type="password"
                        name="confirmPassword"
                        value={this.state.confirmPassword}
                        onChange={(e) => {
                            this.validateConfirmPassword(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.confirmPassword === 'has-success'}
                        invalid={this.state.validate.confirmPassword === 'has-danger'}
                    />
                </FormGroup>

                <Button name="submit" disabled={!this.validateRegistrationForm()}>Sign-up</Button>
            </Form>
        );
    }

    render() {
        return (
            <div>
                <div className="Signup">
                    {this.state.newUser === null ? this.renderForm() : this.renderConfirmationForm()}
                    {<div name="serverError">{this.state.serverError}</div>}
                </div>
            </div>
        );
    }
}