import React, { Component } from 'react';
import { API } from "aws-amplify";
import Post from "../components/Post";

import './News.css'

class News extends Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: []
        };
    }

    componentDidMount() {
        const cached = sessionStorage.getItem('posts');
        if (cached) {
            const posts = JSON.parse(cached);
            this.setState({
                posts: posts
            })
        }

        else {
            API.get('posts', '/posts').then(
                result => {
                    sessionStorage.setItem("posts", JSON.stringify(result));
                    this.setState({
                        posts: result
                    });
                },

                error => {
                    console.log(error);
                }
            );
        }
    }

    renderPosts(posts) {
        const elements = posts.map(post => {
            console.log(post.id.trim());

            return <Post props={this.props} key={post.id.trim()} post = {post}/>;
        });

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        {elements}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <div className="blog-header">
                    <div className="container">
                        <h1 className="blog-title">Updates</h1>
                        <p className="lead blog-description">List of updates will be posted here.</p>
                    </div>
                </div>

                {this.state.posts && this.renderPosts(this.state.posts)}
            </div>
        );
    }
}

export default News;