import React, { Component } from 'react';
import YouTube from 'react-youtube';

import { Link } from "react-router-dom";

class Home extends Component {
    render() {
        const opts = {
            width: '768',
            height: '432',
            playerVars: { // https://developers.google.com/youtube/player_parameters
                controls: 2,
                fs: 1,
                modestbranding: 1,
                showinfo: 0
            }
        };

        return (
            <div>
                <div className="blog-header">
                    <div className="container">
                        <h1 className="blog-title">Welcome to My Awesome Website!</h1>
                        <p className="lead blog-description">Now watch the video</p>
                    </div>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-md-9">
                            <YouTube videoId="O4vsfRNBDmI" opts={opts} onReady={this._onReady}/>
                        </div>

                        <div className="col-md-3">
                            <h3 className="my-3">Some Text Here</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                            <Link to="/download">Download Now!</Link>
                            <h3 className="my-3">And Random List Here</h3>
                            <ul>
                                <li>Lorem Ipsum</li>
                                <li>Dolor Sit Amet</li>
                                <li>Consectetur</li>
                                <li>Adipiscing Elit</li>
                            </ul>
                        </div>

                    </div>

                    <h3 className="my-4">Aaaand Large Description</h3>

                    <div className="row">
                        <div className="col-md-12">
                            <p>Is education residence conveying so so. Suppose shyness say ten behaved morning had. Any unsatiable assistance compliment occasional too reasonably advantages. Unpleasing has ask acceptance partiality alteration understood two. Worth no tiled my at house added. Married he hearing am it totally removal. Remove but suffer wanted his lively length. Moonlight two applauded conveying end direction old principle but. Are expenses distance weddings perceive strongly who age domestic.</p>
                            <p>Material confined likewise it humanity raillery an unpacked as he. Three chief merit no if. Now how her edward engage not horses. Oh resolution he dissimilar precaution to comparison an. Matters engaged between he of pursuit manners we moments. Merit gay end sight front. Manor equal it on again ye folly by match. In so melancholy as an sentiments simplicity connection. Far supply depart branch agreed old get our.</p>
                            <p>Yourself off its pleasant ecstatic now law. Ye their mirth seems of songs. Prospect out bed contempt separate. Her inquietude our shy yet sentiments collecting. Cottage fat beloved himself arrived old. Grave widow hours among him ﻿no you led. Power had these met least nor young. Yet match drift wrong his our. </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    _onReady(event) {
        event.target.pauseVideo();
    }
}

export default Home;