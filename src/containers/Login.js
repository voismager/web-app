import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input, Button, Label, FormText, FormFeedback } from 'reactstrap';
import { Auth } from "aws-amplify";
import config from '../config';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",

            validate: {
                username: '',
                password: ''
            },

            serverError: null
        };
    }

    validate = () => {
        const { validate } = this.state;

        return validate.username === 'has-success' && validate.password === 'has-success';
    };

    validateUsername(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length >= config.app.MIN_USERNAME_L &&
            value.length <= config.app.MAX_USERNAME_L &&
            /^[A-Za-z0-9@_.]+$/.test(value)) {

            validate.username = 'has-success';
        }

        else validate.username = 'has-danger';

        this.setState({ validate });
    }

    validatePassword(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length >= config.app.MIN_PASSWORD_L &&
            value.length <= config.app.MAX_PASSWORD_L &&
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]*$/.test(value)) {

            validate.password = 'has-success';
        }

        else validate.password = 'has-danger';

        this.setState({ validate });
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value,
            serverError: null
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        try {
            const result = await Auth.signIn(this.state.username, this.state.password);
            this.props.userHasAuthenticated(result.username);
            this.props.history.push("/");
        }

        catch (e) {
            this.setState({
                serverError: e.message
            });
        }
    };

    render() {
        return (
            <div className="Login">
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label>Username</Label>
                        <Input
                            type="text"
                            name="username"
                            value={this.state.username}
                            onChange={(e) => {
                                this.validateUsername(e);
                                this.handleChange(e);
                            }}
                            valid={this.state.validate.username === 'has-success'}
                            invalid={this.state.validate.username === 'has-danger'}
                        />
                        <FormFeedback valid>
                            Much better!
                        </FormFeedback>
                        <FormFeedback>
                            Oh! I certainly don't like that! Please input a correct username.
                        </FormFeedback>
                        <FormText>You can use your username or email.</FormText>
                    </FormGroup>

                    <FormGroup>
                        <Label>Password</Label>
                        <Input
                            type="password"
                            name="password"
                            value={this.state.password}
                            onChange={(e) => {
                                this.validatePassword(e);
                                this.handleChange(e);
                            }}
                            valid={this.state.validate.password === 'has-success'}
                            invalid={this.state.validate.password === 'has-danger'}
                        />

                        <FormFeedback valid>
                            Mmm, that's a tasty looking password you've got there.
                        </FormFeedback>
                        <FormFeedback>
                            Nope, no, nah...
                        </FormFeedback>
                    </FormGroup>

                    <Link to="/login/reset">Forgot password?</Link>

                    <Button name="submit" disabled={!this.validate()}>Log-in</Button>
                </Form>

                {<div name="serverError">{this.state.serverError}</div>}
            </div>
        );
    }
}