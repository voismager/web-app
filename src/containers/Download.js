import React, { Component } from 'react';
import { Storage } from "aws-amplify";
import File from "../components/File";

class Download extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            files: null
        };
    }

    async componentDidMount() {
        if (this.props.isAuthenticated) {
            const inStorage = sessionStorage.getItem("files");

            if (inStorage) {
                this.setState({
                    files: JSON.parse(inStorage)
                });
            }

            else {
                try {
                    const result = await Storage.list('');

                    sessionStorage.setItem("files", JSON.stringify(result));
                    this.setState({
                        files: result
                    });
                }

                catch (e) {
                    alert(e);
                }
            }
        }

        this.setState({ isLoading: false });
    }

    renderFiles(files) {
        const elements = files.map(file =>
            <File key={file.key} file = {file}/>
        );

        return (
            <div className="container">
                <h3>Some files:</h3>
                <div className="row">
                    <div className="col-sm-12">
                        {elements}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <div className="blog-header">
                    <div className="container">
                        <h1 className="blog-title">Download Page</h1>
                        <p className="lead blog-description">Here you can download files from public bucket</p>
                    </div>
                </div>

                <div className="container">
                    {this.state.isLoading ? <span>Loading...</span> :
                        this.props.isAuthenticated ?
                            this.renderFiles(this.state.files)
                            :
                            <span>It looks like you need to authorize!</span>
                    }
                </div>
            </div>
        );
    }
}

export default Download;








