import React, { Component } from "react";
import { Auth } from "aws-amplify";
import { Link } from "react-router-dom";
import { Form, FormGroup, Input, Button, Label, FormText } from 'reactstrap';
import config from '../config';
import "./ResetPassword.css";

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            code: "",
            email: "",
            password: "",
            confirmPassword: "",
            codeSent: false,
            confirmed: false,

            validate: {
                email: '',
                password: '',
                confirmPassword: ''
            },

            serverError: null
        };
    }

    validateEmail(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length <= config.app.MAX_EMAIL_L &&
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) {

            validate.email = 'has-success';
        }

        else validate.email = 'has-danger';

        this.setState({ validate });
    }

    validateRequestCodeForm() {
        const { validate } = this.state;

        return validate.email === 'has-success';
    }

    validatePassword(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value.length >= config.app.MIN_PASSWORD_L &&
            value.length <= config.app.MAX_PASSWORD_L &&
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]*$/.test(value)) {

            validate.password = 'has-success';
        }

        else validate.password = 'has-danger';

        this.setState({ validate });
    }

    validateConfirmPassword(e) {
        const { validate } = this.state;

        const value = e.target.value;

        if (value === this.state.password) {
            validate.confirmPassword = 'has-success';
        }

        else validate.confirmPassword = 'has-danger';

        this.setState({ validate });
    }

    validateResetForm() {
        const { validate } = this.state;

        return this.state.code.length > 0
            && validate.password === 'has-success'
            && validate.confirmPassword === 'has-success'
        ;
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value,
            serverError: null
        });
    };

    handleSendCodeClick = async event => {
        event.preventDefault();

        try {
            await Auth.forgotPassword(this.state.email);
            this.setState({ codeSent: true });
        }

        catch (e) {
            switch (e.name) {
                case 'UserNotFoundException':
                    this.setState({
                        serverError: 'User not found.'
                    });

                    break;

                default:
                    this.setState({
                        serverError: e.message
                    });

                    break
            }
        }
    };

    handleConfirmClick = async event => {
        event.preventDefault();

        try {
            await Auth.forgotPasswordSubmit(
                this.state.email,
                this.state.code,
                this.state.password
            );
            this.setState({ confirmed: true });
        }

        catch (e) {
            this.setState({
                serverError: e.message
            });
        }
    };

    renderRequestCodeForm() {
        return (
            <Form onSubmit={this.handleSendCodeClick}>
                <FormGroup>
                    <Label>Email</Label>
                    <Input
                        type="email"
                        name="email"
                        value={this.state.email}
                        onChange={(e) => {
                            this.validateEmail(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.email === 'has-success'}
                        invalid={this.state.validate.email === 'has-danger'}
                    />
                </FormGroup>

                <Button name="submitEmail" disabled={!this.validateRequestCodeForm()}>Send Confirmation</Button>
            </Form>
        );
    }

    renderConfirmationForm() {
        return (
            <Form onSubmit={this.handleConfirmClick}>
                <FormGroup>
                    <Label>Confirmation Code</Label>
                    <Input
                        name="code"
                        value={this.state.code}
                        onChange={this.handleChange}
                    />
                    <FormText>Please check your email ({this.state.email}) for the confirmation code.</FormText>
                </FormGroup>

                <hr />

                <FormGroup>
                    <Label>New Password</Label>
                    <Input
                        type="password"
                        name="password"
                        value={this.state.password}
                        onChange={(e) => {
                            this.validatePassword(e);
                            this.validateConfirmPassword(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.password === 'has-success'}
                        invalid={this.state.validate.password === 'has-danger'}
                    />
                </FormGroup>

                <FormGroup>
                    <Label>Confirm Password</Label>
                    <Input
                        type="password"
                        name="confirmPassword"
                        value={this.state.confirmPassword}
                        onChange={(e) => {
                            this.validateConfirmPassword(e);
                            this.handleChange(e);
                        }}
                        valid={this.state.validate.confirmPassword === 'has-success'}
                        invalid={this.state.validate.confirmPassword === 'has-danger'}
                    />
                </FormGroup>

                <Button name="confirmButton" disabled={!this.validateResetForm()}>Confirm</Button>
            </Form>
        );
    }

    renderSuccessMessage() {
        return (
            <div className="success">
                <p>Your password has been reset.</p>
                <p>
                    <Link to="/login">
                        Click here to login again.
                    </Link>
                </p>
            </div>
        );
    }

    render() {
        return (
            <div className="ResetPassword">
                {!this.state.codeSent
                    ? this.renderRequestCodeForm()
                    : !this.state.confirmed
                        ? this.renderConfirmationForm()
                        : this.renderSuccessMessage()}

                {<div name="serverError">{this.state.serverError}</div>}
            </div>
        );
    }
}