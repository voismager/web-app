import React, { Component } from 'react';
import { Form, FormGroup, Input, Button } from 'reactstrap';
import { API } from "aws-amplify";
import Moment from 'react-moment';
import config from '../config';

class Post extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comments: null,
            comment: ""
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    validateComment() {
        return this.state.comment.length > 0 && this.state.comment.length <= config.app.MAX_COMMENT_L;
    }

    setOpen = () => {
        if (this.state.comments) {
            this.setState({
                comments: null
            });
        }

        else {
            const id = this.props.post.id;
            const cached = sessionStorage.getItem('comments' + id);

            if (cached) {
                this.setState({
                    comments: JSON.parse(sessionStorage.getItem('comments' + id))
                });
            }

            else this.updateComments();
        }
    };

    updateComments = () => {
        const id = this.props.post.id;

        API.get('posts', '/posts/comments/' + id).then(
            result => {
                sessionStorage.setItem('comments' + id, JSON.stringify(result));

                this.setState({
                    comments: result
                });
            },

            error => {
                alert(error);
            }
        );
    };

    postComment = event => {
        event.preventDefault();

        API.put('posts', '/posts/comments', {
            body: {
                content: this.state.comment,
                author: localStorage.getItem("username"),
                id: this.props.post.id
            }
        }).then(
            result => {
                this.setState({
                    comment: ""
                });

                this.updateComments();
            },

            error => {
                console.log(error);
            }
        );
    };

    renderComments(comments) {
        const elements = comments.map(comment =>
            <li key={comment.id} className="media">
                <div className="media-body">
                    <span className="text-muted pull-right">
                        <Moment fromNow>{comment.postedAt}</Moment>
                    </span>

                    <strong className="text-success">{'@' + comment.author}</strong>
                    <p>{comment.body}</p>
                </div>
            </li>
        );

        return (
            <div className="row bootstrap snippets">
                <div className="col-md-6 col-md-offset-5 col-sm-12">
                    <div className="comment-wrapper">
                        <div className="panel">
                            <div className="panel-body">
                                {this.props.props.isAuthenticated ?
                                    <Form onSubmit={this.postComment}>
                                        <FormGroup>
                                            <Input
                                                type="textarea"
                                                name="comment"
                                                value={this.state.comment}
                                                onChange={this.handleChange}
                                                placeholder="write a comment..."
                                            />
                                        </FormGroup>

                                        <Button name="postCommentButton" disabled={!this.validateComment()} className="btn btn-info pull-right">Post</Button>
                                    </Form>
                                    :
                                    <p>Sign In in order to leave a comment!</p>
                                }

                                <div className="clearfix"/>
                                <ul className="media-list">
                                    {elements}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

    render() {
        const { post } = this.props;

        return (
            <div name={this.props.post.id.trim()} className="blog-post">
                <h2 className="blog-post-title">{post.title}</h2>
                <p className="blog-post-meta">Posted on {post.date}</p>
                <div dangerouslySetInnerHTML={{__html: post.body}}/>

                <button name="commentsButton" onClick={this.setOpen}>Comments</button>
                {this.state.comments && this.renderComments(this.state.comments)}
            </div>
        );
    }
}

export default Post;