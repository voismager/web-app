import React, { Component } from 'react';
import { Storage } from "aws-amplify";

class File extends Component {
    constructor(props) {
        super(props);

        this.state = {
            file: null
        };
    }

    async componentDidMount() {
        const {file} = this.props;

        this.setState({
            file: await Storage.get(file.key)
        })
    }

    render() {
        const {file} = this.props;

        return (
            <div>
                {this.state.file ? <a href={this.state.file} download>{file.key}</a> : <div>...</div>}
            </div>
        );
    }
}

export default File;