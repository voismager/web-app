import React, { Component } from 'react';
import { Switch, Route, Link, withRouter } from 'react-router-dom';
import AppliedRoute from './components/AppliedRoute';
import { Auth } from "aws-amplify";
import Home from './containers/Home';
import News from './containers/News';
import Download from './containers/Download';
import Register from './containers/Register';
import Login from "./containers/Login";
import ResetPassword from "./containers/ResetPassword";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            isAuthenticating: true,
            username: null
        };
    }

    async componentDidMount() {
        try {
            await Auth.currentSession();

            this.setState({
                isAuthenticated: true,
                username: localStorage.getItem("username")
            });
        }

        catch(e) {
            if (e !== 'No current user') {
                alert(e);
            }
        }

        this.setState({ isAuthenticating: false });
    }

    userHasAuthenticated = username => {
        localStorage.setItem("username", username);

        this.setState({
            isAuthenticated: true,
            username: username
        });
    };

    handleLogout = async event => {
        await Auth.signOut();

        this.setState({
            isAuthenticated: false,
            username: localStorage.removeItem("username")
        });

        this.props.history.push("/login");
    };

    //  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

    render() {
        const childProps = {
            isAuthenticated: this.state.isAuthenticated,
            userHasAuthenticated: this.userHasAuthenticated
        };

        return (
            !this.state.isAuthenticating &&

            <div className="App">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

                <div className="float-sm">
                    <div className="fl-fl float-fb">
                        <i className="fa fa-facebook" style={{fontSize: '24px'}}/>
                        <a href="/" target="_blank"> Like us!</a>
                    </div>
                    <div className="fl-fl float-tw">
                        <i className="fa fa-twitter" style={{fontSize: '24px'}}/>
                        <a href="/" target="_blank">Follow us!</a>
                    </div>
                    <div className="fl-fl float-rd">
                        <i className="fa fa-reddit" style={{fontSize: '24px'}}/>
                        <a href="/" target="_blank">Talk about us!</a>
                    </div>
                    <div className="fl-fl float-tb">
                        <i className="fa fa-tumblr" style={{fontSize: '24px'}}/>
                        <a href="/" target="_blank">Discover us!</a>
                    </div>
                </div>

                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div className="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/news">Updates</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/download">Download</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="mx-auto order-0">
                        <Link className="navbar-brand mx-auto" to="/">My Awesome Website!</Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                            <span className="navbar-toggler-icon"/>
                        </button>
                    </div>
                    <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                        {this.state.isAuthenticated ?
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <span className="navbar-text" style={{marginRight: '20px'}}>Greetings, {this.state.username}!</span>
                                </li>

                                <li className="nav-item">
                                    <button name="logout" className="btn btn-light" onClick={this.handleLogout}>Logout</button>
                                </li>
                            </ul>
                            :
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <Link className="nav-link" to="/register">Register</Link>
                                </li>

                                <li className="nav-item">
                                    <Link className="nav-link" to="/login">Login</Link>
                                </li>
                            </ul>
                        }
                    </div>
                </nav>

                <main style={{minHeight: 'calc(100vh - 70px)'}}>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <AppliedRoute path='/news' component={News} props={childProps}/>
                        <AppliedRoute path='/download' component={Download} props={childProps}/>
                        <Route path='/login/reset' component={ResetPassword}/>
                        <AppliedRoute path='/register' component={Register} props={childProps}/>
                        <AppliedRoute path='/login' component={Login} props={childProps}/>
                    </Switch>
                </main>

                <footer className="page-footer font-small blue">
                    <div className="footer-copyright text-center py-3">© 2018 Copyright:
                        <a href="https://github.com/gRastaSsS">gRastaSsS</a>
                    </div>


                </footer>
            </div>
        )
    }
}

export default withRouter(App)