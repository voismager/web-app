const { Builder, By, Key, until } = require('selenium-webdriver');
const chrome = require('chromedriver');
const { expect } = require('chai');

const driver = new Builder()
    .forBrowser("chrome")
    .build();

const homeUrl = 'http://localhost:3000/';
const titleName = 'Awesome Website!';

let goHome = function () {
    driver.get(homeUrl);
};

let goTo = function (path) {
    driver.get(homeUrl + path);
};

let checkFieldValidation = async function (name, key, shouldBeValid) {
    const element = await driver.findElement(By.name(name));
    await element.clear();
    await element.sendKeys(key);
    const classAttr = await element.getAttribute('class');
    expect(classAttr).to.include(shouldBeValid ? 'is-valid' : 'is-invalid');
};

let fillField = async function (name, key) {
    const element = await driver.findElement(By.name(name));
    await element.clear();
    await element.sendKeys(key);
};

let clickOn = async function (name) {
    const element = await driver.findElement(By.name(name));
    await element.click();
};

let login = async function () {
    await goTo('login');
    await fillField('username', 'username');
    await fillField('password', 'Qwerty123');
    const button = await driver.findElement(By.name('submit'));
    await button.click();
    await driver.wait(until.urlIs(homeUrl), 20000);
};

describe("1", function() {
    before(async function() {
        await goHome();
    });

    it("1.1", async () => {
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl);
    });

    it("1.2", async () => {
        await driver.findElement(By.linkText('Home')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl);
    });

    it("1.3", async () => {
        await driver.findElement(By.linkText('Updates')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl + 'news');
    });

    it("1.4", async () => {
        await driver.findElement(By.linkText('Download')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl + 'download');
    });

    it("1.5", async () => {
        await driver.findElement(By.linkText('Register')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl + 'register');
    });

    it("1.6", async () => {
        await driver.findElement(By.linkText('Login')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl + 'login');
    });

    it("1.7", async () => {
        await goHome();
        await driver.findElement(By.linkText('Download Now!')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl + 'download');
    });

    it("1.8", async () => {
        await goTo('login');
        await driver.findElement(By.linkText('Forgot password?')).click();
        const currUrl = await driver.getCurrentUrl();
        expect(currUrl).to.equal(homeUrl + 'login/reset');
    });

    it("2.1", async () => {
        await goHome();
        const title = await driver.getTitle();
        expect(title).to.equal(titleName);
    })
});

describe("2", function () {
    before(async function() {
        await goTo('login');
    });

    it("1.1", async () => {
        await checkFieldValidation('username', 'login1', true);
    });

    it("1.2", async () => {
        await checkFieldValidation('username', 'it_is_a_pretty_login_you_got_there_pal_@_wait_what_is_happening', true);
    });

    it("1.3", async () => {
        await checkFieldValidation('username', 'okay_but_seriously_tho_i_am_doing_this_at_4_am_i_feel_like_its_a_safe_place_to_write_some_of_my_private_and_ridiculous_thoughts', true);
    });

    it("1.4", async () => {
        await checkFieldValidation('username', 'login', false);
    });

    it("1.5", async () => {
        await checkFieldValidation('username', 'somebody_plz_remind_me_what_i_am_doing_with_my_life_@_i_remember_i_ve_got_a_plan_before_like_hey_lets_go_@_right_now_@@@@@@@@@@@', false);
    });

    it("1.6", async () => {
        await checkFieldValidation('username', 'logi?', false);
    });

    it("1.7", async () => {
        await checkFieldValidation('username', 'login?', false);
    });

    it("1.8", async () => {
        await checkFieldValidation('username', 'heeeey_@_yall_wanna_meet_me_at_the_sky?_because_i_ll_be_waiting', false);
    });

    it("1.9", async () => {
        await checkFieldValidation('username', 'wow_my_ceiling_just_turned_into_the_floor_???_i_need_more_coffeeeee_@_so_tired_@_that_was_literally_the_worst_year_of_my_life((', false);
    });

    it("1.10", async () => {
        await checkFieldValidation('username', 'my_head_kinda_just_drifted_away_+_at_this_point_i_am_so_high_it_cant_possibly_rain_+_but_i_seek_for_just_a_little_more_elevation', false);
    });

    it("2.1", async () => {
        await checkFieldValidation('password', 'Qwert123', true);
    });

    it("2.2", async () => {
        await checkFieldValidation('password', 'Qwerty13I4probablyShouldStopWritingItButMyThoughtsAreStreamingDirectlyToTheKeyboardMyHandsAreTiedIMeanWhatCouldPossiblyGoWrong', true);
    });

    it("2.3", async () => {
        await checkFieldValidation('password', '123OkayWhatIThinkRightNowIsMyMentalHealthHaveIEverToldYouBoutMyTherapyQuiteHelpfulIGuessButYeahSometimesItAllThatIOnceLostComesBackToMockMeLikeIAmSomeSortOfFreakSeriouslyStopYouPeopleDontKnowWhatYouDoingAndHowItDrivesMeCrazyEveryDayAllIReallyWantIsLetItGo', true);
    });

    it("2.4", async () => {
        await checkFieldValidation('password', 'Qwer123', false);
    });

    it("2.5", async () => {
        await checkFieldValidation('password', '1243GuessThatsWhyTheyAllDidntShowUpImNotAPeoplePleaserIDontHaveAnyExpectationsAndDontCareBoutMeetingExpectationsOfOthersHoldYourRavesOutOfMeLetMePutOnMyHeadphonesAndAndWalkOutOfAllTheProblemsIMeanWhereDidIGoWrongIJustWantToMoveOnIMeanIWishYouWellButLetItGo', false);
    });

    it("2.6", async () => {
        await checkFieldValidation('password', 'qwer123', false);
    });

    it("2.7", async () => {
        await checkFieldValidation('password', 'qwert123', false);
    });

    it("2.8", async () => {
        await checkFieldValidation('password', 'OkayIAmAboutToPassOutSoIWillProbablyStopRightNowJustGiveMeLastLineOnceALittleFishBumpedIntoMeAndISwearHeSaidToMeHolaAmigoItsMeH', false);
    });

    it("2.9", async () => {
        await checkFieldValidation('password', 'jlYggVRwkCXtqgFlihUZXUjXBDWUbyjAKUKQtFNrvmEjrACJCpNETrUFzhjoqdBAVscjArXvPXLeqAdTeyVuoZoInOGpwvATdpLxSgNgMsrsHJDpunIxZmftJoZyBXtwcJMVfNSLqgUlsoOJnZIKABlBrkeKCyFMqLXZSsBZlrNudSArvweOUEMpQSniBpcEMRXaRlTIhFOvNUtJtymFSSzxKXihELsjhcUEkZQnSUHImXbTeHfVRvXCOKLOcqp', false);
    });

    it("2.10", async () => {
        await checkFieldValidation('password', 'LLDlGlneREgHhBfcvpiLBilgyCmLNaFtqdNUFvFmtqJpacYexqZOKrAeyszMDwSkMAMGapGweFyzciKXCJzocJXShbWIoweeMEIdNHOaIDvsruZdtMwGjVofgLztrJwYuOYMMkkiKwDEFFVdManbcSpiOaBDCaVGkfUcDzFoEoLAhyLLhTGGbEfGxFLVNnbwoVVkXRcFUrmiXfgZuXVvtbNQRKXiOPmARSgkMQZBaRKLppSjwAHXpDhcCAZnqBQ', false);
    });

    it("3.1", async () => {
        await fillField('username', 'login1');
        await fillField('password', 'Qwert123');

        const button = await driver.findElement(By.name('submit'));
        const enabled = await button.isEnabled();
        expect(enabled).to.equal(true);
    });

    it("4.1", async () => {
        await fillField('username', 'username1');
        await fillField('password', 'Qwerty123');

        const button = await driver.findElement(By.name('submit'));
        await button.click();

        const errorElement = await driver.findElement(By.name('serverError'));

        await driver.wait(until.elementIsVisible(errorElement), 10000);

        expect(await errorElement.getText()).to.equal('User does not exist.');
    });

    it("4.2", async () => {
        await fillField('username', 'username2');
        await fillField('password', 'Qwerty1234');

        const button = await driver.findElement(By.name('submit'));
        await button.click();

        const errorElement = await driver.findElement(By.name('serverError'));

        await driver.wait(until.elementIsVisible(errorElement), 10000);

        expect(await errorElement.getText()).to.equal('Incorrect username or password.');
    });

    it("4.3", async () => {
        await fillField('username', 'username');
        await fillField('password', 'Qwerty123');

        const button = await driver.findElement(By.name('submit'));
        await button.click();

        await driver.wait(until.urlIs(homeUrl), 10000);

        const logoutElement = await driver.findElement(By.name('logout'));

        expect(await logoutElement.isDisplayed()).to.equal(true);
    });

    it("5.1", async () => {
        await goTo('login/reset');
        await checkFieldValidation('email', 'Diq86zNL>gMxbHpXkR9WrE9f2f0UJjQiebG6uxZHekrBzVTqiSaibeIogC1P>pH', false);
    });

    it("5.2", async () => {
        await goTo('login/reset');
        await checkFieldValidation('email', 'uf7OOCEMJaCOJlEWqeJROA7DpxbsTGWH3ZtwOI180AOGyFJhhpXwGcf2IIkUjT69AMO6@bvGQIPg2j6irB34bRFXqvZdQPXJI9HovWDxt8dHCxGzV9mSqwIO4giLQpvOIhtjfEsSmNZ8cXrAr5z73R2wROdkl4LA1MOwmnZsyOVEYYv7gLGoqL@L@Qx6klHqZIvNI4RrM8mbNWsaY4m3zXlqJgLVJ1CwF1n014GJPQ5sY48cq1Ew8tSIgsknPcY', false);
    });

    it("5.3", async () => {
        await goTo('login/reset');
        await checkFieldValidation('email', 'thatIsAVeryVeryVeryVeryVeryVeryVeryVeryLongEmailUGot@hehhas.com', true);
    });

    it("5.4", async () => {
        await goTo('login/reset');
        await checkFieldValidation('email', 'thatIsAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongEmailUGot@hekkas.com', true);
    });

    it("5.5", async () => {
        await goTo('login/reset');

        await fillField('email', 'not@exist.hk');

        const button = await driver.findElement(By.name('submitEmail'));
        await button.click();

        const errorElement = await driver.findElement(By.name('serverError'));

        await driver.wait(until.elementIsVisible(errorElement), 10000);

        expect(await errorElement.getText()).to.equal('User not found.');
    });

    it("5.6", async () => {
        await goTo('login/reset');

        await fillField('email', 'grastasss@gmail.com');

        const button = await driver.findElement(By.name('submitEmail'));
        await button.click();

        await driver.wait(until.elementLocated(By.name('password')), 10000);

        await checkFieldValidation('password', 'Qwerty12', true);
    });

    it("6.1", async () => {
        await checkFieldValidation('password', 'Qwerty12', true);
    });

    it("6.2", async () => {
        await checkFieldValidation('password', '6N2nTRDHG2tUhGeIW9r2JL2f1qh6MRk1Dp658QCSCB98Q8P1vZykCi3GiUmYY5er9keffIkg0C9OFJIGq0mI5HGdL7PrFiqSqZ0ysEpeR1aPyITPylw9E9DM07R8S7X', true);
    });

    it("6.3", async () => {
        await checkFieldValidation('password', '69RaDpETlUTIeAFFbhYwyvJFFhcoU1LSE183xfqGAWdk49KNLSAsdEwdP7h3Rdczgd2VxXhppZwfYEhsn1xRtyeb3hCQGaz9W2ThMIdGO54qRul97JOgECz06cB1kxybVpsOzCgcJr1jaFC0hPFz4tCtFm0q7hFb938iO1IbtEoeb6y7Y5Oeq7jzeZbQH7hVMNoPfSZVRmCXrIOf2Uq5bnlt8ScJCc07gYFk9Miz1fPUzzCcjVnuKqF1PZriKEa', true);
    });

    it("6.4", async () => {
        await checkFieldValidation('password', 'nItWuKF', false);
    });

    it("6.5", async () => {
        await checkFieldValidation('password', '17yl1zCEkx8f66y9RJa7PiPxKLPQhMRfvrlvawj9BGz0lapO21hm05FlZ9Q1je2PsGece7H6iH47G4RO8MnVOms2gxU7wiSv6ZtLaYXp4uZYbPT5R40MY7358lZC3jN5NESzzQ0Sp4bBu2lYXSKEZLpP8M0YC3ITpniA7dc8FE5hewgpxw1hU8nArzZEM12liF0NXazf0cZodBSR59sRYq9sK12aaJrcrX3rC8qKmLiO6oUE0QXSy8PmQlKWiuvb', false);
    });

    it("6.6", async () => {
        await checkFieldValidation('password', 'MD5W7?y', false);
    });

    it("6.7", async () => {
        await checkFieldValidation('password', 'MD5W7?y1', false);
    });

    it("6.8", async () => {
        await checkFieldValidation('password', 'eXFZULPfmhZK6pI7QWn7X1uz?TPGz5reukVDKQRzvAYozLs4SUFTfA9kNshpr8Vead7FHE?Q24lANe6XHs3797OzBcfliSpnZOKIELmEamfs9reNiDQpCzqYlDf?Vxr', false);
    });

    it("6.9", async () => {
        await checkFieldValidation('password', 'Ka4jVlXDyqzINStBQwQocFl6UKTE@GE57Y9n8Tl1?@2pLfzN??LvLO59pLgGYcClGCxenJolL9Cfk3?XVbXUZ3JcYA?yu9cjRocJsbpC62WQXB1V?NhM3rsxfSC8wmmdIPlE9ebDSUhmKtBtABlfM3UranfI5myJehaEnroMgFYpmj59sQMYSNRb8tNbLfxvVan1fDq6C8ZRtXXVnQ5txGZHpCMMDsbptmYK63OYdye@QfInvxnaXLOOoQNBmNz', false);
    });

    it("6.10", async () => {
        await checkFieldValidation('password', 'Ka4jVlXDyqzINStBQwQocFl6UKTE@GE57Y9n8Tl1?@2pLfzN??LvLO59pLgGYcClGCxenJolL9Cfk3?XVbXUZ3JcYA?yu9cjRocJsbpC62WQXB1V?NhM3rsxfSC8wmmdIPlE9ebDSUhmKtBtABlfM3UranfI5myJehaEnroMgFYpmj59sQMYSNRb8tNbLfxvVan1fDq6C8ZRtXXVnQ5txGZHpCMMDsbptmYK63OYdye@QfInvxnaXLOOoQNBmNz1', false);
    });

    it("7.1-2", async () => {
        await fillField('password', 'Qwerty123');
        await checkFieldValidation('confirmPassword', '', false);
    });

    it("7.3-4", async () => {
        await fillField('password', 'Qwerty1234');
        await checkFieldValidation('confirmPassword', 'Qwerty123', false);
    });

    it("7.5-6", async () => {
        await fillField('password', 'Qwerty123');
        await checkFieldValidation('confirmPassword', 'Qwerty123', true);
    });

    it("8.1", async () => {
        await fillField('password', 'Qwerty123');
        await fillField('confirmPassword', 'Qwerty123');

        const button = await driver.findElement(By.name('confirmButton'));

        expect(await button.isEnabled()).to.equal(false);
    });

    it("8.2", async () => {
        await fillField('password', 'Qwerty123');
        await fillField('confirmPassword', 'Qwerty123');
        await fillField('code', '1');

        const button = await driver.findElement(By.name('confirmButton'));

        expect(await button.isEnabled()).to.equal(true);
    });
});

describe("3", function() {
    before(async function() {
        await goTo('register');
    });

    it("1.1", async () => {
        await checkFieldValidation('username', 'myName', true);
    });

    it("1.2", async () => {
        await checkFieldValidation('username', 'e4PLyhgoUQizMsjoMIvbdGLbwQ2AjvYJZdD4Mj0H_dV7geUrujdeLurf6AETeo5', true);
    });

    it("1.3", async () => {
        await checkFieldValidation('username', 'FScDtKGMbq6wC3Znfke0O1WZc77ZEF_9CdovCEEqNw3whE7IBEYKLzjLEv1LS5VgIEf7Kibegy8FNgKkI6FaNEGljDpWQAri0Wri2UX6uTH7ko6RqAV_mJP3jn9hZiR', true);
    });

    it("1.4", async () => {
        await checkFieldValidation('username', 'iName', false);
    });

    it("1.5", async () => {
        await checkFieldValidation('username', 'FScDtKGMbq6wC3Znfke0O1WZc77ZEF_9CdovCEEqNw3whE7IBEYKLzjLEv1LS5VgIEf7Kibegy8FNgKkI6FaNEGljDpWQAri0Wri2UX6uTH7ko6RqAV_mJP3jn9hZiRa', false);
    });

    it("1.6", async () => {
        await checkFieldValidation('username', 'iNam/', false);
    });

    it("1.7", async () => {
        await checkFieldValidation('username', 'iNam/e', false);
    });

    it("1.8", async () => {
        await checkFieldValidation('username', 'IaJ20vclT1WeTsbSgjBRC.FZDPoBUACOm5YmOKP91A1If9mOEWV0X8MMWYzOXqy', false);
    });

    it("1.9", async () => {
        await checkFieldValidation('username', 'LGKbmjOMbPby0NpHdKwOSe19KNxbcqNrsqMCdTDz3udg.P5OIuVyHag96KP6TinmZ9RTyje,BA4qGzSHOh_N.8VCQlCW42jSHE33.amMZf2zWBtkZ,IQ1bjQGnw9FL5', false);
    });

    it("1.10", async () => {
        await checkFieldValidation('username', '39PRSmIof99IC_kA.4AqEjCOA.BYAvrWuB1KBUuC7caa3SFfsKMKK,1M8yfH5cl5fGOTK16P.FJ4St,or4VZffWZIlX8krWwrDrd.g_CAoV5T7QNb6PRjxZCJw98gPix', false);
    });

    it("2.1", async () => {
        await checkFieldValidation('email', 'thatIsAVeryVeryVeryVeryVeryVeryVeryVeryLongEmailUGot@hehhas.com', true);
    });

    it("2.2", async () => {
        await checkFieldValidation('email', 'thatIsAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongEmailUGot@hekkas.com', true);
    });

    it("2.3", async () => {
        await checkFieldValidation('email', 'thatIsAAVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongEmailUGot@hekkas.com', false);
    });

    it("2.4", async () => {
        await checkFieldValidation('email', 'X5invfLyIiVmD554vqYrsbap14yllxZ0Mx1014AzivAxjhBUKk6uRC7eKUllME3', false);
    });

    it("2.5", async () => {
        await checkFieldValidation('email', 'MzQkE3L150HzVl7E8Ua0L8J_KWyAE6oabzvckrr07CTEyV3qQM9K45HQjawygkNoYG59U8D5OC3.Yy6p8pZGxCdJ6vWY1nqqZ,eMVNuTaH12oYp3XQpRqivHo0fuFNmhMsJL6twvD5oxx6b5McStyyH_Xt1wSbe.jHCVjAeTcFhU5Dx3ImoDSHAThxG8vWLZMOHKq7Cw2mXuQot4AVoXfrc1c3fvLytCwZxe,L6mrM_0302HgHBGA8HMz0QCyJ', false);
    });

    it("3.1", async () => {
        await checkFieldValidation('password', 'Qwert123', true);
    });

    it("3.2", async () => {
        await checkFieldValidation('password', '5kHf6AABTBpqYEC5ZabwJqYAOpzd62P3OnkkkZh0d0uGDOGQlA47kRPllHDy2FFumBqGlstaiaBDS01gY04ZLlhelUatinJCI5xRWXJjNkAIog6pmhRvEIpOd9x5cKn', true);
    });

    it("3.3", async () => {
        await checkFieldValidation('password', '6TaAj2yPZ7yTNflkDOORQtLiQyf1QPvFnibSR8jEthQElHUWADtAVh6LqjHY35QyEwBmyfT5plSRV6IuokhlCnR9no3BNubfbNIhd44CGD2b0MxSpZmcVOYhrKU7COGr4HQ5GiaPLKSXe1trCixGyhCwE2HkW0m7LwuTwmyFVva7cWPzja9FV2uxFYJe7iwyUUPLjffGaxPbgmWXcuBjN9vMWA94rQTAWaePk3wHYdtCyNuAeJRNdS1bztEcorb', true);
    });

    it("3.4", async () => {
        await checkFieldValidation('password', 'Qwer123', false);
    });

    it("3.5", async () => {
        await checkFieldValidation('password', 'pKyes5yD4aDTpC99UA2107qfuokwadmvvJTopHwS8npVfIULvxmJjXIBAP1VHZwKMoM5iOmlwrjQQpdnUcMpDSyzsv8Sqfv8jeEDQCcnTKyOklcNGLh4B9EOLXfH8s2wO7S5T3RVtH7TIrWayHqzB98iBWzOLN8RztfoDiyeKPcAcWT4yg1UDFCtecumup3lqzTrnIUkDQManMknyiCIwTPDqa7RD3XBhLLUCAo7d7lSLjCAF6w6g6bzu8gOqMT4', false);
    });

    it("3.6", async () => {
        await checkFieldValidation('password', 'qwer123', false);
    });

    it("3.7", async () => {
        await checkFieldValidation('password', 'qwert123', false);
    });

    it("3.8", async () => {
        await checkFieldValidation('password', 'f7J171ly5ZD2OH3Fj1qEZsmzeWhmcJAREE9dJ7vBBcr6LZP,-q0UD-fXUH,m1Hndl6YR2Ge2j6PN6Rw5MY4.j,chADieAbQum9ZR1RbvqOMj4E0gBn86SuFaZN8h8Bm', false);
    });

    it("3.9", async () => {
        await checkFieldValidation('password', 'HPgxEcdPWkXA7umvz7fILjYU98RSBPnpA4YNckoSVRgvP53hbFO2ko-XSx,zI-UuhopwfzIvgMQC-Jqm75gZrfeq,CwvlwAAUDCoaOJtCBB-cBHvHfIbYo3nzJLH8f0bR5IXLiR5yLuV65Y4Y--e9i21Vnl,3E159qChUhJtSmAVKfJ3ySp6jxph3Xgy7ZwMqRwU.dS5hmHxWNpMRt29QxTyYcDuYW1npYQ5I.idVkiK4gApOyFa.5fdn-vQoMG', false);
    });

    it("3.10", async () => {
        await checkFieldValidation('password', '8AyhDE19ka1CheRlzSUhna7wUXF8ktbmIysh80zllp8vnTG50eJ3d70GvV,AMnUvcTSo-MbyzQ36j2k51wXeW5r9qL.VkbutICHKbMzxvA2v5WpxdRVn9Ax1YITCWqg.jwRAv,snRJweoqZ6yPmJvdCkGOSSk2Cnx7bzjXbKxBffvtq90M33MOKhEayDapRdwiG5efDRVNPVMbFYXHFG,UIiA6kTdbD.WbwZIiCUA6d1YjvFm6uUp6muGyj07uV.', false);
    });

    it("4.1-2", async () => {
        await fillField('password', 'Qwerty123');
        await checkFieldValidation('confirmPassword', '', false);
    });

    it("4.3-4", async () => {
        await fillField('password', 'Qwerty1234');
        await checkFieldValidation('confirmPassword', 'Qwerty123', false);
    });

    it("4.5-6", async () => {
        await fillField('password', 'Qwerty123');
        await checkFieldValidation('confirmPassword', 'Qwerty123', true);
    });

    it("5.1", async () => {
        await fillField('username', 'username');
        await fillField('email', 'email@email.com');
        await fillField('password', 'Qwerty123');
        await fillField('confirmPassword', 'Qwerty123');

        const button = await driver.findElement(By.name('submit'));
        expect(await button.isEnabled()).to.equal(true);
    });

    it("6.1", async () => {
        await fillField('username', 'username');
        await fillField('email', 'username1@users.com');
        await fillField('password', 'Qwerty123');
        await fillField('confirmPassword', 'Qwerty123');

        const button = await driver.findElement(By.name('submit'));
        await button.click();

        const errorElement = await driver.findElement(By.name('serverError'));

        await driver.wait(until.elementIsVisible(errorElement), 10000);

        expect(await errorElement.getText()).to.equal('Username is already taken!');
    });

    it("6.2", async () => {
        await fillField('username', 'username5');
        await fillField('email', 'username@users.com');
        await fillField('password', 'Qwerty123');
        await fillField('confirmPassword', 'Qwerty123');

        const button = await driver.findElement(By.name('submit'));
        await button.click();

        const errorElement = await driver.findElement(By.name('serverError'));

        await driver.wait(until.elementIsVisible(errorElement), 10000);

        expect(await errorElement.getText()).to.equal('Email already exists.');
    });

    it("6.3", async () => {
        await fillField('username', 'username');
        await fillField('email', 'username@users.com');
        await fillField('password', 'Qwerty123');
        await fillField('confirmPassword', 'Qwerty123');

        const button = await driver.findElement(By.name('submit'));
        await button.click();

        const errorElement = await driver.findElement(By.name('serverError'));

        await driver.wait(until.elementIsVisible(errorElement), 10000);

        expect(await errorElement.getText()).to.equal('Email already exists.');
    });
});

describe("4", function () {
    before(async function() {
        await goTo('login');
    });

    it("1.1", async () => {
        await login();

        await clickOn('logout');

        const registerElement = await driver.findElement(By.linkText('Register'));
        const loginElement = await driver.findElement(By.linkText('Login'));

        await driver.wait(until.elementIsVisible(registerElement), 10000);

        expect(await loginElement.isDisplayed()).to.equal(true);
    });
});

describe("5", function () {
    after(function() {
        return driver.quit();
    });

    it("2.1", async () => {
        await login();
        await driver.findElement(By.linkText('Updates')).click();

        await driver.wait(until.elementLocated(By.name('a5eb1516-a27f-4333-93c5-edc118bb2763'), 10000));

        const post = await driver.findElement(By.name('a5eb1516-a27f-4333-93c5-edc118bb2763'));

        await post.findElement(By.name('commentsButton')).click();

        await driver.wait(until.elementLocated(By.name('comment'), 10000));

        const commentArea = await post.findElement(By.name('comment'));
        await commentArea.clear();

        const button = await post.findElement(By.name('postCommentButton'));

        expect(await button.isEnabled()).to.equal(false);
    });

    it("2.2", async () => {
        const post = await driver.findElement(By.name('a5eb1516-a27f-4333-93c5-edc118bb2763'));
        const commentArea = await post.findElement(By.name('comment'));
        await commentArea.clear();
        await commentArea.sendKeys('sUmSDikfwNlWYU4m5ngCrBGkWmwmPXParvuKhhG2zD8h2RHOvU9oykCEUigAspQYdaToMxsGR8TE91DzPJSHV9Ws7cI54kTTwP7QFLedaohNGd8j68CBOlPaOwgNacEMHBlFQdPIX3JS7SeW61b9xHtMboQqb5zYBVWZkkLBBGepSdT8EiKZRXvOaiSbPsVbNs7DZLI5V2fI1EreddQcd7kWdZp6OKG3Y6jhev1m4LpSJhS3m');

        const button = await post.findElement(By.name('postCommentButton'));

        expect(await button.isEnabled()).to.equal(false);
    });

    it("2.3", async () => {
        const post = await driver.findElement(By.name('a5eb1516-a27f-4333-93c5-edc118bb2763'));
        const commentArea = await post.findElement(By.name('comment'));
        await commentArea.clear();
        await commentArea.sendKeys('m');

        const button = await post.findElement(By.name('postCommentButton'));

        expect(await button.isEnabled()).to.equal(true);
    });

    it("2.4", async () => {
        const post = await driver.findElement(By.name('a5eb1516-a27f-4333-93c5-edc118bb2763'));
        const commentArea = await post.findElement(By.name('comment'));
        await commentArea.clear();
        await commentArea.sendKeys('E7jsd7jWW1IBHrNOP6qDhqe0W3d0u3puwxAW3Ze7KJrqcJ210naBZHh6FkdWaukYkfHRwlbnO7ijKoHgvGqOnfumo0PL2fVT89yNTQOMzHLrxjakahKdAqWM');

        const button = await post.findElement(By.name('postCommentButton'));

        expect(await button.isEnabled()).to.equal(true);
    });

    it("2.5", async () => {
        const post = await driver.findElement(By.name('a5eb1516-a27f-4333-93c5-edc118bb2763'));
        const commentArea = await post.findElement(By.name('comment'));
        await commentArea.clear();
        await commentArea.sendKeys('2XYrdPnszbYnV22ja2ecpzBVgaVzOUfTosZ5NomnjI3T5jEgMM8PoJNsoKwbQC2jEUhPBsyc1xIVztPvXeMcWQ8i8nlAAXeKEooURLSxZLDGhFuFVGxGt3lgyGfc1qtPk0gLmsv6dJwYYEsg9ROhCjiMfBb2cEJq8h4Wj4v5zPaiuKy17eLXQCsY1p0wRPqig9LZ2Su50YTfhqmLPzeBodU39HZ9QKvZuhXnH5URqngMSSgt');

        const button = await post.findElement(By.name('postCommentButton'));

        expect(await button.isEnabled()).to.equal(true);
    });
});

